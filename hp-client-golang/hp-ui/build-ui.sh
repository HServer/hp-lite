fyne-cross windows --app-id=cn.hpproxy --output hp-pro-ui-windows
fyne-cross linux --app-id cn.hpproxy --output hp-pro-ui-linux
fyne-cross darwin --arch=amd64 --app-id=cn.hpproxy --output hp-pro-ui-apple-amd
fyne-cross darwin --arch=arm64 --app-id=cn.hpproxy --output hp-pro-ui-arm
