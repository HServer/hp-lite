package net.hserver.hplite.domian.bean;

import lombok.Data;

@Data
public class ReqLoginUser {

    private String email;

    private String password;
}
