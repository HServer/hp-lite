package net.hserver.hplite.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.hserver.hplite.domian.entity.UserCustomEntity;

@Mybatis
public interface UserCustomDao extends BaseMapper<UserCustomEntity> {

}
