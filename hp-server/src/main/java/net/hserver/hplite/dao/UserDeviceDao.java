package net.hserver.hplite.dao;

import cn.hserver.plugin.mybatis.annotation.Mybatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.hserver.hplite.domian.entity.UserDeviceEntity;

@Mybatis
public interface UserDeviceDao extends BaseMapper<UserDeviceEntity> {

}
